addEvent(window, 'load', inicializarEventos, false);

function inicializarEventos(){
  var ob;
  for (f = 1; f <= 16; f++) {
    ob = document.getElementById('enlace' + f);
    addEvent(ob, 'click', presionEnlace, false);
  }
}

function presionEnlace(e){
  if (window.event) {
    window.event.returnValue=false;
    var url = window.event.srcElement.getAttribute('href');
    cargar(url);     
  }
  else if (e) {
         e.preventDefault();
         var url = e.target.getAttribute('href');
         cargar(url);     
       }
}

var conexion1;

function cargar(url){
  if (url != '') {
    conexion1=crearXMLHttpRequest();
    conexion1.onreadystatechange = procesarEventos;
    conexion1.open("GET", url, true);
    conexion1.send(null);
  }
}

function procesarEventos(){
  var detalles = document.getElementById("foto");
  if(conexion1.readyState == 4)
    detalles.innerHTML = conexion1.responseText;
  else detalles.innerHTML = "<img src='img/cargando.gif' alt='' />";
}


function addEvent(elemento,nomevento,funcion,captura){
  if (elemento.attachEvent) {
    elemento.attachEvent('on'+nomevento,funcion);
    return true;
  }
  else if (elemento.addEventListener) {
         elemento.addEventListener(nomevento,funcion,captura);
         return true;
       }
       else return false;
}

function crearXMLHttpRequest(){
  var xmlHttp = null;
  if (window.ActiveXObject) 
    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
  else if (window.XMLHttpRequest) 
    xmlHttp = new XMLHttpRequest();
  return xmlHttp;
}

<?php
include_once('config.php');
header('Content-Type: text/html; charset=ISO-8859-1');

$host     = DB_HOST;
$username = DB_USER;
$password = DB_PASSWORD;
$dbname   = DB_NAME;

// La BD debe contener una tabla llamada "fotos" con dos campos: cod y ruta
// El campo cod es un entero que se va a usar para hacer la consulta
// El campo ruta es la ruta de una imagen, por ejemplo "img/perro.gif"
 
$con = mysqli_connect($host, $username, $password, $dbname);

if (mysqli_connect_errno()) {
  echo "Error de MySQL: " . mysqli_connect_error();
} else {
  $codigo = $_REQUEST['cod'];
  $query = "SELECT * FROM fotos WHERE cod=" . $codigo;
  $resultado = mysqli_query($con, $query);

  if (!$resultado) {
    echo "Error de MySQL: " . $query;
  } else {
    $fila = mysqli_fetch_assoc($resultado);
    $ruta = $fila['ruta'];
    $file = '../' . $ruta;
    if (file_exists($file)) {
        $ruta .= '?time=' . filemtime($file);
    }
    echo "<img src='" . $ruta . "' alt='' />";

    mysqli_free_result($resultado);
  }
}

mysqli_close($con);

?>
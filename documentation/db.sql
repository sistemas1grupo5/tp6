-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: fdb16.awardspace.net
-- Generation Time: Jun 21, 2017 at 05:57 PM
-- Server version: 5.7.18-log
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `2348048_db1`
--

-- --------------------------------------------------------

--
-- Table structure for table `fotos`
--

CREATE TABLE `fotos` (
  `cod` int(11) NOT NULL,
  `ruta` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fotos`
--

INSERT INTO `fotos` (`cod`, `ruta`) VALUES
(1, 'img/01_01.jpg'),
(2, 'img/01_02.jpg'),
(3, 'img/01_03.jpg'),
(4, 'img/01_04.jpg'),
(5, ' img/02_01.jpg'),
(6, 'img/02_02.jpg'),
(7, 'img/02_03.jpg'),
(8, 'img/02_04.jpg'),
(9, 'img/03_01.jpg'),
(10, 'img/03_02.jpg'),
(11, 'img/03_03.jpg'),
(12, 'img/03_04.jpg'),
(13, 'img/04_01.jpg'),
(15, 'img/04_03.jpg'),
(16, 'img/04_04.jpg'),
(14, 'img/04_02.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fotos`
--
ALTER TABLE `fotos`
  ADD PRIMARY KEY (`cod`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
